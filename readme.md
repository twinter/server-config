# Server setup notes

Originally created and tested with an ubuntu 18.04 server.

Some helpful notes are below the setup instructions.

## setup and customisation

### install helpful tools

`apt-get install htop glances`

### create own user

- create new user with `useradd -m tom`.
- add user to sudo group with `usermod -a -G sudo tom`
- edit the sudoers file with `visudo` and make sure the following line is included: `%sudo ALL=(ALL:ALL) NOPASSWD: ALL` to disabled password prompts for members of the sudo group
- set a password for the user with `passwd tom`
- don't forget to also change the password of the root user

### setup ssh

- add ssh key to the server by running `ssh-copy-id -i public_key.pub tom@<server address>` on the connecting computer. You may need to write your public key in a file (public_key.pub in the example). Alternatively you can log on to the server and add you public key manually to ~/.ssh/authorized_keys
- edit /etc/ssh/sshd_config to contain the following lines:
	- `PermitRootLogin no`
	- `PasswordAuthentication no`
- restart the ssh daemon with `sudo systemctl restart sshd`

### configure automatic upgrades

- install automatic upgrades with `sudo apt install unattended-upgrades`
- configure it as described here: https://help.ubuntu.com/community/AutomaticSecurityUpdates (the most important command is to enable it with `sudo dpkg-reconfigure --priority=low unattended-upgrades`)

### create ssh key for the server

Create a new key pair with `ssh-keygen` and copy the public key to whatever other services you need (git remotes, other servers you may need to ssh into from this server, ...).

### get the config repo

This is a dotfiles repo based as described here: https://www.atlassian.com/git/tutorials/dotfiles
Use `config` instead of git when working with the repo.
Run the following steps to use it on another system:

```
# use this if you don't have a ssh key for gitlab: https://gitlab.com/twinter/server-config.git
git clone --bare git@gitlab.com:twinter/server-config.git $HOME/.cfg
```

This first command will likely cause a prompt to import gitlabs fingerprint for the ssh connection that's created.
You can find the fingerprint of gitlab here: https://docs.gitlab.com/ee/user/gitlab_com/index.html#ssh-host-keys-fingerprints

Then run the following steps:

```
function config {
   /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}
mkdir -p .config-backup
config checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Backing up pre-existing dot files.";
    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}
fi;
config config --local status.showUntrackedFiles no
config checkout
```

### zsh

Antigen config details: https://github.com/zsh-users/antigen

List of zsh plugins: https://github.com/unixorn/awesome-zsh-plugins and https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins

Installation:
```
# install zsh and plugin manager
sudo apt-get update -y
sudo apt-get install -y zsh git

# install fonts for zsh
sudo apt-get install -y powerline fonts-powerline

# install antigen
mkdir .antigen
curl -L git.io/antigen > ~/.antigen/antigen.zsh

sudo usermod -s /usr/bin/zsh $(whoami)  # to set zsh as the default shell
```

My .zshrc file and the powerlevel10k config file (.p10k.zsh) are included in the repo.

### docker

Described here: https://docs.docker.com/install/linux/docker-ce/ubuntu/

Don't forget to also install docker-compose

### fail2ban

Run `sudo apt-get install fail2ban` to install it.

There is no further configuration required if you just want it to protect sshd.

### docker-compose systemd service

Described here: https://gitlab.com/twinter/docker-compose-systemd-service

### reverse proxy

Described here: https://gitlab.com/twinter/docker-compose-reverse-proxy



## notes

### sshfs as root but login as another user

`sshfs tom@your.server:/ sshfs -o sftp_server="/usr/bin/sudo /usr/lib/openssh/sftp-server"`

### watch threadcount on system

`watch -n 1 -d "ps -elfT | wc -l"`

### git config

git config --global remote.origin.prune true

git config --global init.defaultBranch main
